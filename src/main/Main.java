package main;

import java.util.List;
import java.util.concurrent.Callable;

import connection.Ats;
import exceptions.PhoneCreationException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import phone.Aon;
import phone.Iphone;
import phone.Phone;
import phone.Yealink;
import phone.internals.CaseColor;
import phone.internals.Ringtone;
import phone.utility.AbstractPhoneFactory;
import phone.utility.ConcretePhoneFactory;
import phone.utility.PhoneModel;

public class Main {
	public static Input input = new ConsoleInput();
	private List<Phone> phones = new ArrayList<>();
	private AbstractPhoneFactory phoneFactory = new ConcretePhoneFactory();

	public static void main(String[] args) {
		Ats ats = Ats.getInstance();
		Main main = new Main();
		main.start();
	}

	public void start() {
		while (true) {
			System.out.println("Please, choose action");
			System.out.println("1 - create phone");
			System.out.println("2 - phone connect/ disconnect");
			System.out.println("3 - call");
			System.out.println("i - show phone info");
			System.out.println("e - exit");
			switch (input.getText().toLowerCase()) {
			case "1":
				createPhone();
				break;
			case "2":
				phoneConnection();
				break;
			case "3":
				call();
				break;
			case "i":
				showInfo();
				break;
			case "e":
				System.out.println("bye-bye");
				return;
			default:
				System.out.println("input not corrected");
			}
		}
	}

	private void call() {
		Phone phone1 = selectPhone();
		Phone phone2 = selectPhone();
		if (phone1.equals(phone2)) {
			System.out.println("You can't call to yourself.");
			return;
		}
		Ats.getInstance().link(phone1.getNumber(), phone2.getNumber());
	}

	private void showInfo() {
		if (phones.size() == 0) {
			System.out.println("No phones created");
			System.out.println();
			return;
		}

		Collections.sort(phones, new Comparator<Phone>() {
			@Override
			public int compare(Phone phone1, Phone phone2) {
				return phone1.getNumber().compareTo(phone2.getNumber());
			}
		});
		
		for (Phone phone : phones) {
			System.out.println(phone);
		}
		System.out.println();
	}

	private Phone selectPhone() {
		Phone phone = null;
		System.out.println("Select a phone.");
		StringBuilder sBuilder = new StringBuilder();
		for (int i = 0; i < phones.size(); i++) {
			sBuilder.append(i + 1);
			sBuilder.append(" - ");
			sBuilder.append(phones.get(i));
			sBuilder.append("\n");
		}
		sBuilder.append("c - cancel");
		System.out.println(sBuilder);
		String input = this.input.getText().toLowerCase();
		int number = 0;
		try {
			number = Integer.parseInt(input);
		} catch (NumberFormatException e) {
			if (!input.equals("c")) {
				System.out.println("incorrect input");
			}
			return null;
		}
		if (number < 1 || number > phones.size()) {
			System.out.println("incorrect number");
			return null;
		}
		return phones.get(number - 1);
	}

	private void createPhone() {
		Phone phone = null;
		System.out.println("input phone model");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < PhoneModel.values().length; i++) {
			sb.append(i + 1);
			sb.append(" - ");
			sb.append(PhoneModel.values()[i]);
			sb.append("\n");
		}
		sb.append("c - cancel");
		System.out.println(sb);
		String input = this.input.getText().toLowerCase();
		int number = 0;
		try {
			number = Integer.parseInt(input);
		} catch (NumberFormatException e) {
			if (!input.equals("c")) {
				System.out.println("incorrect input");
			}
			return;
		}
		if (number < 1 || number > PhoneModel.values().length) {
			System.out.println("incorrect number");
			return;
		}
		phone = phoneFactory.createPhone(PhoneModel.values()[number - 1]);
		generateNumber(phone);
		phones.add(phone);
		System.out.println("Phone successfully created");
	}

	private void generateNumber(Phone phone) {
		phone.setNumber(String.valueOf(1000 + (int) (Math.random() * 9000)));
	}

	public void phoneConnection() {
		Phone phone = selectPhone();
		if (Ats.getInstance().disconnectPhone(phone)) {
			System.out.println("Phone is disconnected");
		} else {
			Ats.getInstance().connectPhone(phone);
			System.out.println("Phone is connected");
		}

	}
}
