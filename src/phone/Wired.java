package phone;

import phone.internals.CaseColor;
import phone.internals.Ringtone;

public abstract class Wired extends Phone {
	public Wired(CaseColor caseColor) {
		super(new WiredRingtoneImplementation(), caseColor);
	}
}
