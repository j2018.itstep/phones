package phone.utility;

import exceptions.PhoneCreationException;
import main.ConsoleInput;
import main.Input;
import phone.Aon;
import phone.Iphone;
import phone.Phone;
import phone.Yealink;
import phone.Zopo;
import phone.internals.CaseColor;
import phone.internals.Ringtone;

public class ConcretePhoneFactory extends AbstractPhoneFactory {

	public ConcretePhoneFactory() {
		super(new ConsoleInput());
	}

	@Override
	public Phone createPhone(PhoneModel model) {
		CaseColor caseColor = CaseColor.values()[(int) (Math.random() * CaseColor.values().length)];
		switch (model) {
		case AON:
			return new Aon(caseColor);
		case IPHONE:
			return new Iphone(caseColor);
		case YEALINK:
			return new Yealink(caseColor);
		case ZOPO:
			return new Zopo(new Ringtone() {
				
				@Override
				public String getRingtone() {
					return "allo";
				}
			}, "", 0, 0, "", caseColor);
		default:
			throw new RuntimeException("No such model");
		}
	}
}
